import { TextInput, View, StyleSheet, Button, Text } from "react-native";
import React, { useState } from "react";
import CheckBox from '@react-native-community/checkbox';

const App = () => {
    const [input1, setInput1] = useState(0);
    const [input2, setInput2] = useState(0);
    const [input3, setInput3] = useState(0);
    const [result, setResult] = useState(0);
    const [checked1, setChecked1] = useState(false);
    const [checked2, setChecked2] = useState(false);
    const [checked3, setChecked3] = useState(false);
    

    const handleOperator = operator => {
        if (checked1 && checked2 && checked3) {
            switch (operator) {
                case 'add':
                    setResult(input1 + input2 + input3);
                    break;
                case 'subtract':
                    setResult(input1 - input2 - input3);
                    break;
                case 'multiply':
                    setResult(input1 * input2 * input3);
                    break;
                case 'divide':
                    setResult(input1 / input2 / input3);
                    break;
                default:
                    setResult(0);
            }
        } else if (checked1 && checked2) {
            switch (operator) {
                case 'add':
                    setResult(input1 + input2);
                    break;
                case 'subtract':
                    setResult(input1 - input2);
                    break;
                case 'multiply':
                    setResult(input1 * input2);
                    break;
                case 'divide':
                    setResult(input1 / input2);
                    break;
                default:
                    setResult(0);
            }
        } else if (checked1 && checked3) {
            switch (operator) {
                case 'add':
                    setResult(input1 + input3);
                    break;
                case 'subtract':
                    setResult(input1 - input3);
                    break;
                case 'multiply':
                    setResult(input1 * input3);
                    break;
                case 'divide':
                    setResult(input1 / input3);
                    break;
                default:
                    setResult(0);
            }
        } else if (checked2 && checked3) {
            switch (operator) {
                case 'add':
                    setResult(input2 + input3);
                    break;
                case 'subtract':
                    setResult(input2 - input3);
                    break;
                case 'multiply':
                    setResult(input2 * input3);
                    break;
                case 'divide':
                    setResult(input2 / input3);
                    break;
                default:
                    setResult(0);
            }
        } else {
            setResult('Error: Please check at least 2 inputs');
        }
    };

    return (
        <View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <TextInput
                    style={styles.input}
                    value={input1.toString()}
                    onChangeText={value => value != "" ? setInput1(parseInt(value)) : setInput1(value)}
                    placeholder="angka 1"
                />
                <CheckBox
                    value={checked1}
                    onValueChange={() => setChecked1(!checked1)}
                    style={styles.checkbox}
                />
            </View>

            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <TextInput
                    style={styles.input}
                    value={input2.toString()}
                    onChangeText={value => value != "" ? setInput2(parseInt(value)) : setInput2(value)}
                    placeholder="angka 2"
                />
                <CheckBox
                    value={checked2}
                    onValueChange={() => setChecked2(!checked2)}
                    style={styles.checkbox}
                />
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <TextInput
                    style={styles.input}
                    value={input3.toString()}
                    onChangeText={value => value != "" ? setInput3(parseInt(value)) : setInput3(value)}
                    placeholder="angka 3"
                />
                <CheckBox
                    value={checked3}
                    onValueChange={() => setChecked3(!checked3)}
                    style={styles.checkbox}
                />
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <View style={{ flex: 1, marginRight: 20, marginLeft: 20 }}>
                    <Button
                        onPress={() => handleOperator('add')}
                        title="+"
                    />
                </View>
                <View style={{ flex: 1, marginRight: 20, marginLeft: 20 }}>
                    <Button
                        onPress={() => handleOperator('subtract')}
                        title="-"
                    />
                </View>
                <View style={{ flex: 1, marginRight: 20, marginLeft: 20 }}>
                    <Button
                        onPress={() => handleOperator('multiply')}
                        title="*"
                    />
                </View>
                <View style={{ flex: 1, marginRight: 20, marginLeft: 20 }}>
                    <Button
                        onPress={() => handleOperator('divide')}
                        title="/"
                    />
                </View>
            </View>
            <TextInput
                style={{ height: 40,
                    margin: 12,
                    borderWidth: 1,
                    padding: 10}}
                editable={false} selectTextOnFocus={false}
                value={result.toString()}
            />
        </View>

    );
};

const styles = StyleSheet.create({
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        padding: 10,
        flex: 1
    },
});

export default App;
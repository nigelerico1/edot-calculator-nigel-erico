import { Image, Text, TextInput, View, StyleSheet, ScrollView } from "react-native";
import React, { useState } from "react"
import anime from './image.jpg'

import { Colors } from "react-native/Libraries/NewAppScreen";

const App = () => {
    return (
        <View>
            <ScrollView>
                <Belajar1 />
                <Belajar2 />
                <Belajar2 />
                <Belajar2 />
                <Photo />
            </ScrollView>
        </View>

    );
};

const Tes = () => {
    return (
        <View>
            <Text>Nigel</Text>
        </View>
    );
};

const Belajar2 = () => {
    return (
        <View style={{ padding: 12, margin: 16, backgroundColor: "#f2f2f2", width: 212, borderRadius: 8 }}>
            <Image
                source={anime}
                style={{ width: 188, height: 107, borderRadius: 8 }}
            />
            <Text
                style={{
                    fontSize: 14,
                    fontWeight: 'bold',
                    color: 'black',
                    marginTop: 16
                }}>
                New Macbook Pro 2019
            </Text>
            <Text
                style={{
                    fontSize: 12,
                    fontWeight: 'bold',
                    color: '#f2994a',
                    marginTop: 12
                }}>
                Rp 25.000.000
            </Text>
            <Text
                style={{
                    fontSize: 12,
                    fontWeight: '300',
                    color: 'black',
                    marginTop: 12
                }}>
                Jakarta Barat
            </Text>
            <View
                style={{
                    backgroundColor: "#6FCF97",
                    paddingVertical: 6,
                    borderRadius: 25,
                    marginTop: 16
                }}>
                <Text
                    style={{
                        fontSize: 14,
                        fontWeight: '600',
                        color: 'white',
                        textAlign: 'center'
                    }}>
                    BELI
                </Text>
            </View>
        </View>
    );
};

const Belajar1 = () => {

    const [text, setText] = useState('tes');

    return (
        <View>
            <View style={{ width: 80, height: 80, backgroundColor: '#4287f5' }} />
            <Text>Prawito</Text>
            <Tes />
            <Text>Prawito</Text>
            <Text>Prawito</Text>
            <Text>Prawito</Text>
            <Photo />
            <TextInput
                style={{ height: 40 }}
                placeholder="Type here to translate!"
                onChangeText={newText => setText(newText)}
                defaultValue={text}
            />
            <Text style={{ padding: 10, fontSize: 42 }}>
                {text
                    .split(' ')
                    .map(word => word && '🍕')
                    .join(' ')}
            </Text>
        </View>
    );
};



const Photo = () => {
    return (
        <Image source={{ uri: "https://i.pinimg.com/originals/62/3a/a8/623aa8f9933ee9a286871bf6e0782538.jpg" }} style={{ width: 80, height: 80 }} />
    );
};

const styles = StyleSheet.create({
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        padding: 10,
    },
});

export default App;